package com.algo.BFS;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

public class Graph { // graph is a tree traversal , issue is traversal can be cyclic

	private int verticies;

	private Queue<Integer>[] adjacantNodes;

	public Graph(int v) {
		this.verticies = v;
		adjacantNodes = new LinkedList[v];
		for (int i = 0; i < v; i++) {
			adjacantNodes[i] = new LinkedList<Integer>();
		}
	}

	public void addEdge(int vertex, int weight) {
		adjacantNodes[vertex].add(weight);
	}

	public void applyBFS(int startElement) {

		boolean visited[] = new boolean[verticies];
		Queue<Integer> analysisQueue = new LinkedList<Integer>();

		visited[startElement] = true;
		analysisQueue.add(startElement);

		while (!analysisQueue.isEmpty()) {
			int head = analysisQueue.poll();

			System.out.print(head + " ");

			Iterator<Integer> iterator = adjacantNodes[head].iterator();
			while (iterator.hasNext()) {
				Integer s = iterator.next();
				if (!visited[s]) {
					visited[s] = true;
					analysisQueue.add(s);

				}
			}

		}

	}

	public static void main(String[] args) {

		Graph graph = new Graph(4);

		graph.addEdge(0, 1);
		graph.addEdge(0, 2);
		graph.addEdge(1, 2);
		graph.addEdge(2, 0);
		graph.addEdge(2, 3);
		graph.addEdge(3, 3);

		graph.applyBFS(0);

	}

}
