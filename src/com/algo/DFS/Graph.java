package com.algo.DFS;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Stack;

public class Graph {

	private int verticies;

	private LinkedList<Integer> adjaceantNodes[];

	private Stack<Integer> stack = new Stack<Integer>();

	public Graph(int v) {
		this.verticies = v;
		adjaceantNodes = new LinkedList[v];
		for (int i = 0; i < v; i++) {
			adjaceantNodes[i] = new LinkedList<Integer>();
		}
	}

	public void applyDFS(int startEl) {
		boolean visited[] = new boolean[verticies];
		applyDFSRecur(startEl, visited);
	}

	private void applyDFSRecur(int startEl, boolean[] visited) {

		if (!visited[startEl]) {
			visited[startEl] = true;
		}
		stack.push(startEl);

		// popping out
		int s = stack.pop();

		System.out.println(" " + s);

		LinkedList<Integer> edges = adjaceantNodes[s];
		Iterator<Integer> iterator = edges.iterator();
		while (iterator.hasNext()) {
			Integer edge = (Integer) iterator.next();
			if (!visited[edge])
				applyDFSRecur(edge, visited);

		}

	}

	void addEdge(int v, int w) {
		adjaceantNodes[v].add(w); // Add w to v's list.
	}

	public static void main(String args[]) {
		Graph g = new Graph(6);

		g.addEdge(0, 1);
		g.addEdge(0, 2);
		g.addEdge(1, 2);
		g.addEdge(2, 0);
		g.addEdge(2, 3);
		g.addEdge(3, 3);

		System.out.println("Following is Depth First Traversal " + "(starting from vertex 2)");

		g.applyDFS(2);
	}

}