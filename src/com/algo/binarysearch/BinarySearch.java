/**
 * 
 */
package com.algo.binarysearch;

/**
 * @author nitkulsh
 *
 */
public class BinarySearch {

	public static void main(String[] args) {

		int a[] = { 4, 5, 8, 9, 11, 15, 17 };
		int target = 19;

		int result = searchIndex(a, target);
		System.out.println("Index found  "+result);
	}

	private static int searchIndex(int[] a, int target) {
		int l = 0;
		int h = a.length - 1;

		return performBinarySearch(l, h, a, target);

	}

	private static int performBinarySearch(int l, int h, int[] a, int target) {
		
		if(l==h) {
			if(a[l]==target)
				return l;
			else 
				return -1;
		}

		int mid = (l + h) / 2;
		if (a[mid] == target)
			return mid;

		else if (a[mid] < target)
			return performBinarySearch(mid + 1, h, a, target);
		else
			return performBinarySearch(l, mid - 1, a, target);

	}

}
