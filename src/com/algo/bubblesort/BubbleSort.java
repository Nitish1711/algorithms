package com.algo.bubblesort;

public class BubbleSort {

	public static void main(String[] args) {
		int a[] = { 5, 3, 8, 1, 4, 2 };

		applyBubbleSort(a);

		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i] + " ");
		}
	}

	private static void applyBubbleSort(int[] a) {
		for (int i = 0; i < a.length; i++) {
			for (int j = i + 1; j < a.length; j++) {
				if (a[i] > a[j]) {
					swap(i, j, a);
				}
			}
		}

	}

	private static void swap(int i, int j, int[] a) {
		int temp = a[i];
		a[i] = a[j];
		a[j] = temp;

	}

}
