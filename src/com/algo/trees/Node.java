package com.algo.trees;

public class Node {

	int key;

	Node left;

	Node right;

	public Node(int item) {
		this.key = item;
		left = null;
		right = null;
	}

}
