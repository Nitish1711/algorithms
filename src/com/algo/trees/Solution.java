package com.algo.trees;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Scanner;

public class Solution {

    // Complete the bfs function below.
    static int[] bfs(int n, int m, int[][] edges, int s) {
       List<Integer> finalCost=new ArrayList<>();

        Map<Integer,Integer> nodeWeightsMap=new HashMap<>();
        nodeWeightsMap.put(s,0);
        Queue<Integer> analysisQueue=new LinkedList<Integer>();
        boolean visited[]=new boolean[n];
        visited[s]=true;
        analysisQueue.add(s);
        while(!analysisQueue.isEmpty()){
            Integer currentEl=analysisQueue.poll();
           for(int i=0;i<edges[currentEl].length;i++){
               if(!visited[edges[currentEl][i]]){
                   visited[edges[currentEl][i]]=true;
                   analysisQueue.add(edges[currentEl][i]);
                    if(nodeWeightsMap.containsKey(currentEl)){
                       Integer distanceFromStart=nodeWeightsMap.get(currentEl)+6;
                        finalCost.add(distanceFromStart);
                        nodeWeightsMap.put(edges[currentEl][i],distanceFromStart);
                    }else{
                        Integer distanceFromStart=6;
                        finalCost.add(distanceFromStart);
                        nodeWeightsMap.put(edges[currentEl][i],distanceFromStart);

                    }
               }
           }

        }
        if(finalCost.size()<n-1){
            Integer delta=(n-1)-finalCost.size();
            for(int i=1;i<=delta;i++){
                finalCost.add(-1);
            }
        }
        
    return finalCost.stream().mapToInt(i -> i).toArray();

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int q = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int qItr = 0; qItr < q; qItr++) {
            String[] nm = scanner.nextLine().split(" ");

            int n = Integer.parseInt(nm[0]);

            int m = Integer.parseInt(nm[1]);

            int[][] edges = new int[m][2];

            for (int i = 0; i < m; i++) {
                String[] edgesRowItems = scanner.nextLine().split(" ");
                scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

                for (int j = 0; j < 2; j++) {
                    int edgesItem = Integer.parseInt(edgesRowItems[j]);
                    edges[i][j] = edgesItem;
                }
            }

            int s = scanner.nextInt();
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            int[] result = bfs(n, m, edges, s);

            for (int i = 0; i < result.length; i++) {
                bufferedWriter.write(String.valueOf(result[i]));

                if (i != result.length - 1) {
                    bufferedWriter.write(" ");
                }
            }

            bufferedWriter.newLine();
        }

        bufferedWriter.close();

        scanner.close();
    }
}
