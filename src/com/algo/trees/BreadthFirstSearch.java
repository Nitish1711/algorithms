package com.algo.trees;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

public class BreadthFirstSearch {  //level order of a tree

	int verticies;

	static Set<Integer> bfsSet = new HashSet<Integer>();

	List<Integer>[] edgeHoldingArray;// each element holds a list of all its connected nodes in a graph

	public BreadthFirstSearch(int totalVerticies) {
		this.verticies = totalVerticies;
		edgeHoldingArray = new ArrayList[verticies];
		for (int i = 0; i < verticies; i++) {
			edgeHoldingArray[i] = new ArrayList<Integer>();
		}
	}

	public void addEdge(int vertex, int connectedVertex) {
		edgeHoldingArray[vertex].add(connectedVertex);
	}

	public static void main(String[] args) {
		BreadthFirstSearch bfs = new BreadthFirstSearch(4);
		bfs.addEdge(0, 1);
		bfs.addEdge(0, 2);
		bfs.addEdge(1, 2);
		bfs.addEdge(2, 0);
		bfs.addEdge(2, 3);
		bfs.addEdge(3, 3);
		System.out.println(bfsSet.toString());
	}

	private void applyBFS(int strtingVtx) {
		boolean visited[] = new boolean[verticies];
		Queue<Integer> exploringQueue = new LinkedList<Integer>();
		visited[strtingVtx] = true; // visit vertex
		exploringQueue.add(strtingVtx);// start exploring
		while (!exploringQueue.isEmpty()) {
			Integer vertexTobeExplored = exploringQueue.poll(); // remove for exploring
			bfsSet.add(vertexTobeExplored);
			edgeHoldingArray[vertexTobeExplored].forEach(vtx -> {
				if (!visited[vtx]){
					exploringQueue.add(vtx);
					visited[vtx] = true;
				}
			});
		}
	}

}
