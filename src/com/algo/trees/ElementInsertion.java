package com.algo.trees;

import java.util.Optional;

public class ElementInsertion {
	
	 Node root;
	
	public ElementInsertion() {
		root = null;
	}
	
	public static void main(String[] args) {
		
		ElementInsertion obj=new ElementInsertion();
		
		obj.insert(20);
		obj.insert(35);
		
		obj.insert(10);
		obj.insert(23);
		obj.insert(24);
		obj.insert(21);
		obj.insert(12);
		
		obj.print();
		
	}
	
	public void print() {
		printInorder(root);
	}
	
	
	 void printInorder(Node root) {
		if(root!=null) {
			printInorder(root.left);
			System.out.println(root.key);
			printInorder(root.right);
		}
		
	}
	
	

	private void insert(int newItem) {
		root=insertPreOrder(root,newItem);
		
	}

	private Node insertPreOrder(Node root, int newItem) {
		if (Optional.ofNullable(root).isEmpty()) {
			root = new Node(newItem);
			return root;
		} else if (root.key == newItem) {
			return root;
		} else if (root.key > newItem) {
			root.left=insertPreOrder(root.left, newItem);
			return root;
		} else if (root.key < newItem) {
			root.right=insertPreOrder(root.right, newItem);
			return root ;
		}

		return null;
	}

}
