/**
 * 
 */
package com.algo.trees;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * @author nitkulsh
 *
 */
public class DepthFirstSearch {
	
	int verticies;
	
	List<Integer>[] edgeConnectingArr;
	
	boolean visited[];
	Stack<Integer> stack=new Stack<Integer>();

	
	@SuppressWarnings("unchecked")
	public DepthFirstSearch(int totalVertex) {
		this.verticies=totalVertex;
		edgeConnectingArr=new ArrayList[totalVertex];
		for(int i=0;i<verticies;i++) {
			edgeConnectingArr[i]=new ArrayList<Integer>();
		}
		visited=new boolean[verticies];
		
	}
	
	public static void main(String[] args) {
		
		
		DepthFirstSearch dfs=new DepthFirstSearch(4);
		
		dfs.addEdge(0, 1);
		dfs.addEdge(0, 2);
		dfs.addEdge(1, 2);
		dfs.addEdge(2, 0);
		dfs.addEdge(2, 3);
		dfs.addEdge(3, 3);
		dfs.applyDFS(2);
	}

	private void applyDFS(int startEl) {
		if (!visited[startEl]) {
			visited[startEl] = true;
			stack.push(startEl);
			Integer visitedEl = stack.pop();
			System.out.println(" " + visitedEl);
			edgeConnectingArr[visitedEl].forEach(vtx -> {
				applyDFS(vtx);
			});
		}

	}

	private void addEdge(int vertex, int connectNode) {
		edgeConnectingArr[vertex].add(connectNode);
	}

}
