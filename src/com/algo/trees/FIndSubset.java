package com.algo.trees;

import java.util.ArrayList;
import java.util.List;

public class FIndSubset {

	public static void main(String[] args) {
		int nums[]= {1,2};
		subsets(nums);
	}

	
    public static List<List<Integer>> subsets(int[] nums) {
        
        List<List<Integer>> results=new ArrayList<>();
        List<Integer> subset=new ArrayList();
        
        findSubSet(nums,results,subset,0);
        
        return results;
    }
    
    private static  void findSubSet(int nums[],List<List<Integer>> results,List<Integer> subset,int startIndex){
        
        results.add(new ArrayList<>(subset));
        for(int i=startIndex;i<nums.length;i++){
            subset.add(nums[i]);
            findSubSet(nums,results,subset,i+1);
            subset.remove(subset.size()-1);
        }
    }

}
