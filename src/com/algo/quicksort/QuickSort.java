package com.algo.quicksort;

public class QuickSort {

	public static void main(String[] args) {

		int a[] = { 3,2,1,5,6,4 };
		int low = 0;
		int high = a.length - 1;

		applyQuickSort(low, high, a);

		for (int i = 0; i < a.length; i++)
			System.out.print(a[i] + " ");
	}

	private static void applyQuickSort(int low, int high, int[] a) {		
		if (low < high) {
			int j = partitionUsingQuickSort(low, high, a);
			applyQuickSort(low, j, a);
			applyQuickSort(j + 1, high, a);
		}

	}

	private static int partitionUsingQuickSort(int low, int high, int[] a) {

		int i = low;
		int j = high;
		int pivot = a[low];

		while (i < j) {

			while (pivot >= a[i]) {
				i++;
			}

			while (a[j] > pivot) {
				j--;
			}

			if (i < j)
				swap(i, j, a);

		}
		swap(low, j, a);

		return j;
	}

	private static void swap(int i, int j, int[] a) {
		int temp = a[i];
		a[i] = a[j];
		a[j] = temp;

	}
}
