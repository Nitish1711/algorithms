/**
 * 
 */
package com.algo.heap;

import java.util.ArrayList;
import java.util.List;

/**
 * @author nitkulsh
 *
 */
public class AddElementInMaxHeap {

	public static void main(String[] args) {
		List<Integer> maxHeapList = new ArrayList<Integer>();
		maxHeapList.add(9);
		maxHeapList.add(8);
		maxHeapList.add(5);
		maxHeapList.add(3);
		maxHeapList.add(2);
		maxHeapList.add(4);
		maxHeapList.add(1);

		int[] maxHeap = addElement(maxHeapList, 6);

		for (int i = 0; i < maxHeap.length; i++) {
			System.out.print(maxHeap[i] + " ");
		}

	}

	private static int[] addElement(List<Integer> maxHeapList, int element) {
		int temp = element;
		maxHeapList.add(temp);
		int maxHeapArr[] = maxHeapList.stream().mapToInt(i -> i).toArray();
		int n = maxHeapArr.length - 1;
		int i = n;
		while (i > 0 && temp > maxHeapArr[i / 2]) {
			maxHeapArr[i] = maxHeapArr[i / 2];
			i = i / 2;
		}
		maxHeapArr[i] = temp;

		return maxHeapArr;
	}

}
