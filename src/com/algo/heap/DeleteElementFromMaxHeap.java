/**
 * 
 */
package com.algo.heap;

import java.util.ArrayList;
import java.util.List;

/**
 * @author nitkulsh
 *
 */
public class DeleteElementFromMaxHeap {
	
	public static void main(String[] args) {
		List<Integer> maxHeapList = new ArrayList<Integer>();
		maxHeapList.add(40);
		maxHeapList.add(25);
		maxHeapList.add(35);
		maxHeapList.add(10);
		maxHeapList.add(5);
		maxHeapList.add(20);
		maxHeapList.add(30);
		int maxHeapArr[] = maxHeapList.stream().mapToInt(i -> i).toArray();

		
		//deleteElement(maxHeapArr,7);
		/*
		 * deleteElement(maxHeapArr,6); deleteElement(maxHeapArr,5);
		 * deleteElement(maxHeapArr,4); deleteElement(maxHeapArr,3);
		 * deleteElement(maxHeapArr,2);
		 */		int remainMaxHeap[]=deleteElement(maxHeapArr,7);
		for (int i = 0; i < remainMaxHeap.length; i++) {
			System.out.print(remainMaxHeap[i] + " ");
		}


	}
//if taken as i=0; parent at i-1/2 , rchild 2i+2, l child 2i+1
	private static int[] deleteElement(int[] aHeap,int heapSize) {
		int x=aHeap[0];
		aHeap[0]=aHeap[heapSize-1];
		aHeap[heapSize-1]=-1;  //put default value in last element to be vacated
		int i=0;
		int j=2*i+1;// left child picked , right would be 2i+1

		while (j < heapSize - 2) {  // loop starts from zero
			if (aHeap[j + 1] > aHeap[j]) {
				j = j + 1;
			}
			if(aHeap[j]>aHeap[i]) {
				int tempvar=aHeap[i];
				aHeap[i]=aHeap[j];
				aHeap[j]=tempvar;
				i=j;
				j=2*i+1;// left child pointing
			}else
				break;

		}
		return aHeap;
	}

}
