/**
 * 
 */
package com.algo.heap;

/**
 * @author nitkulsh
 *
 */
public class HeapSort {

	static int nextFreeSpace = 0;

	public static void main(String[] args) {

		int[] sourceArr = { 35, 25, 30, 10, 5, 20, 40 }; // to be inserted in a heap
		int maxHeapArray[] = new int[sourceArr.length];
		for (int i = 0; i < sourceArr.length; i++) {
			insertInAHeap(sourceArr[i], maxHeapArray);
		}

		for (int j = 0; j < maxHeapArray.length; j++) {
			deleteFromHeap(maxHeapArray, maxHeapArray.length - j);
		}
		System.out.print("Heap Sort : ");
		for (int k = 0; k < maxHeapArray.length; k++) {
			System.out.print(maxHeapArray[k] + " ");
		}
        String str = "discuss.leetcode.com"; 
        String[] arrOfStr = str.split("\\.",2); 
        System.out.println(arrOfStr[1]);
        
        String string = "This+is+test string on web";
        String splitData[] = string.split("\\+", 2);
        System.out.println(splitData[1]);



	}

	private static void deleteFromHeap(int[] maxHeapArray, int heapSize) {
		int delNumber = maxHeapArray[0];
		maxHeapArray[0] = maxHeapArray[heapSize - 1];
		maxHeapArray[heapSize - 1] = delNumber;
		int i = 0;
		int child = 2 * i + 1; //left child 

		while (child < heapSize - 2) {
			if (maxHeapArray[child + 1] > maxHeapArray[child]) {
				child = child + 1;
			}

			if (maxHeapArray[i] < maxHeapArray[child]) {
				int tempVar = maxHeapArray[i];
				maxHeapArray[i] = maxHeapArray[child];
				maxHeapArray[child] = tempVar;
				i = child;
				child = 2 * i + 1;
			} else {
				break;
			}

		}

	}

	private static void insertInAHeap(int element, int[] maxHeapArray) {
		int i = nextFreeSpace++;
		int temp = element;

		while (i > 0 && temp > maxHeapArray[(i - 1) / 2]) {
			maxHeapArray[i] = maxHeapArray[(i - 1) / 2];
			i = (i - 1) / 2;
		}
		maxHeapArray[i] = temp;
	}

}
