/**
 * 
 */
package com.algo.heap;

/**
 * @author nitkulsh
 *
 */
public class CreateMinHeap {
	
	static int[] sourceArr= {2,9,4,3,5,8};
	
	
	static int  nextFreeSpace=0;
	
	
	
	public static void main(String[] args) {
		int[] maxHeapArr=new int[sourceArr.length];
		for(int i=0;i<sourceArr.length;i++) {
			insertInHeap(sourceArr[i],maxHeapArr);
			
			
		}
		
		for(int i=0;i<maxHeapArr.length;i++) {
			System.out.print(maxHeapArr[i]+" ");
		}
		
	}

	private static void insertInHeap(int targetElement, int[] maxHeapArr) {
		int i = nextFreeSpace++;
		int temp = targetElement;
		
		//child at i parent at i/2 or parent at i then l.child at 2i and r.child at 2i+1
		
		while (i > 0 && temp < maxHeapArr[i / 2]) {
			maxHeapArr[i] = maxHeapArr[i / 2];
			i = i / 2;
		}
		maxHeapArr[i] = temp;

	}
	
	

}
