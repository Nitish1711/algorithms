import java.util.HashMap;

public class MainClass {
	public static void main(String[] args) {
		HashMap<Employee, Integer> hm = new HashMap<Employee, Integer>();
		Employee e1 = new Employee(1, "Nitish");
		Employee e2 = new Employee(2, "nick");
		hm.put(e1, 7);
		e1.setId(5);

		hm.put(e2, e2.getId());
		System.out.println(hm.get(e1));

	}

}
