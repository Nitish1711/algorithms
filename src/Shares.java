import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Shares {
	
	public static void main(String[] args) {
		// 4 5 2 9 17 6
		/*
		 * List<Integer> prices1=new ArrayList<Integer>(); prices1.add(2);
		 * prices1.add(1); prices1.add(6); prices1.add(5);
		 * 
		 * List<Integer> prices2=new ArrayList<Integer>(); prices2.add(6);
		 * prices2.add(55); prices2.add(3); prices2.add(51);
		 * 
		 * List<Integer> prices3=new ArrayList<Integer>(); prices3.add(61);
		 * prices3.add(51); prices3.add(21); prices3.add(11);
		 * 
		 */		Map<String, List<Integer>> sharesHolderMap=new HashMap<String, List<Integer>>();
		
 

	    Integer S1[]={4,8,13,9,7,10,12,14};
	    Integer S2[]={4,2,3,7,9,13,10,12};
	    Integer S3[]={4,6,8,6,9,8,9,15};
	    
	    List<Integer> prices1 = Arrays.asList(S1);
	    List<Integer> prices2 = Arrays.asList(S2);
	    List<Integer> prices3 = Arrays.asList(S3);

	    sharesHolderMap.put("S1", prices1);
		sharesHolderMap.put("S2", prices2);
		sharesHolderMap.put("S3", prices3);

			
		giveRecommendations(sharesHolderMap);
	}
	
	/*
	 * S1-buy d1 S1-sell d3 S2-buy d4 S2-sell d6 S3-buy d7 S3-sell d8
	 * 
	 * profit- 21
	 */

	private static void giveRecommendations(Map<String, List<Integer>> sharesHolderMap) {
		
		int totalprofit=0;
		int holdingCost=sharesHolderMap.get("S1").get(0);
		System.out.println("Share bought initial "+holdingCost);
		for(Map.Entry<String, List<Integer>> entry : sharesHolderMap.entrySet()){
			int shareProfit=Integer.MIN_VALUE;
			int daysCounter=1;
			Map<Integer, Integer> daysHolderMap=new HashMap<Integer, Integer>();
			int minCp=entry.getValue().get(0); 
			int maxCp= Integer.MIN_VALUE;
			
			for(Integer price:entry.getValue()) {
				daysHolderMap.put(price,daysCounter);
				shareProfit=Math.max(shareProfit, price-minCp);
				minCp=Math.min(minCp, price);
				maxCp=Math.max(maxCp, price);
				daysCounter++;

			}
			if((holdingCost==0 || holdingCost>minCp)) {
				System.out.println("Buy "+entry.getKey()+" on day "+daysHolderMap.get(minCp));
				totalprofit=totalprofit+(holdingCost-minCp);
				holdingCost=minCp;
			}
			
			if(holdingCost<maxCp && holdingCost!=0) {
				System.out.println("Sell "+entry.getKey()+" on day "+daysHolderMap.get(maxCp));
				totalprofit=totalprofit+(maxCp-holdingCost);
				holdingCost=0;
			} 

		}	
		System.out.println("total profit "+totalprofit);
 	}

}
