import java.util.HashMap;

public class Employee {
	
	int id;
	String name ;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	public Employee(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/*
	 * @Override public int hashCode() { final int prime = 31; int result = 1;
	 * result = prime * result + id; return result; }
	 * 
	 * @Override public boolean equals(Object obj) { if (this == obj) return true;
	 * if (obj == null) return false; if (getClass() != obj.getClass()) return
	 * false; Employee other = (Employee) obj; if (id != other.id) return false;
	 * return true; }
	 */
}
